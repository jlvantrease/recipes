//
//  ViewController.m
//  Recipes
//
//  Created by Lyndy Tankersley on 2/8/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()



@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    recipeList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RecipeList" ofType:@"plist"]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return recipeList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* dict = recipeList[indexPath.row];
    cell.textLabel.text= dict[@"title"];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"detailSegue" sender: indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath* index = (NSIndexPath*)sender;
    if([segue.identifier isEqualToString:@"detailSegue"]){
        RecipeDetailsController *rdc = (RecipeDetailsController *)segue.destinationViewController;
        //rdc.testText = @"This works";
        rdc.recipeInfo = recipeList[index.row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
