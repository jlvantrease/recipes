//
//  RecipeDetails.h
//  Recipes
//
//  Created by Lyndy Tankersley on 2/8/17.
//  Copyright © 2017 Jason Vantrease. All rights

#import <UIKit/UIKit.h>


@interface RecipeDetails : UIViewController
{
    IBOutlet UIImageView *imageView;
    
    IBOutlet UILabel *ingredientsLbl;
    
    IBOutlet UILabel *instructionLbl;
}


@end
