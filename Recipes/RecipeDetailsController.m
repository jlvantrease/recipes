//
//  RecipeDetailsController.m
//  Recipes
//
//  Created by Lyndy Tankersley on 2/8/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "RecipeDetailsController.h"

@interface RecipeDetailsController ()

@end

@implementation RecipeDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //_testLbl.text = _testText;
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.titleLbl.text = self.recipeInfo[@"title"];
    self.ingredientsLbl.text = self.recipeInfo[@"ingredients"];
    [self.ingredientsLbl sizeToFit];
    self.instructionsLbl.text = self.recipeInfo[@"instructions"];
    [self.instructionsLbl sizeToFit];
    
    NSData * imageInfo = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: self.recipeInfo[@"image"]]];
    self.imageView.image = [UIImage imageWithData: imageInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
