//
//  RecipeDetailsController.h
//  Recipes
//
//  Created by Lyndy Tankersley on 2/8/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeDetailsController : UIViewController
//@property (strong,nonatomic) NSString* testText;
//@property (strong, nonatomic) IBOutlet UITextField *testLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSDictionary* recipeInfo;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *ingredientsLbl;
@property (strong, nonatomic) IBOutlet UILabel *instructionsLbl;

@end
