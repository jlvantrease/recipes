//
//  ViewController.h
//  Recipes
//
//  Created by Lyndy Tankersley on 2/8/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeDetailsController.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray* recipeList;
}


@end

